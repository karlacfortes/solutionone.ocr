﻿using System.Web;
using System.Web.UI.HtmlControls;

namespace SolutionsOne.OCR.Web.Models
{
    public class ProcessViewModel
    {
        public HttpPostedFileBase File { get; set; }
        public Cpf Cpf { get; set; }
    }
}