﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;
using Microsoft.Ajax.Utilities;
using SolutionsOne.OCR.Web.Models;
using Tesseract;

namespace SolutionsOne.OCR.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(HttpPostedFileBase file)
        {
            var processModel = new ProcessViewModel();
             
            if (file != null)
            {
                /*A pasta tessdata armazena os arquivos com os dados aprendidos das linguagens que o sistema for suportar.
                O Tesseract possui um acervo extenso de linguagens suportadas, mas para esse sistema, utilizaremos somente
                o arquivo em português.*/

                using (var engine = new TesseractEngine(Server.MapPath(@"~/tessdata"), "por", EngineMode.Default))
                {
                    var image = new Bitmap(file.InputStream);
                    var pix = PixConverter.ToPix(image);

                    using (var page = engine.Process(pix))
                    {
                        var result = page.GetText().Split('\n');
                        processModel.Cpf = GetCpfInfo(result);
                    }
                }
            }
            
            return View(processModel);
        }

        public Cpf GetCpfInfo(string[] info)
        {
            var result = info.ToList();

            return new Cpf
            {
                Name = result[result.IndexOf(result.Find(r => r.Contains("Nome")))+1],
                RegisterNumber = result.Find(r => Regex.IsMatch(r.Replace(" ", ""), "([0-9]{3}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[-]?[0-9]{2})")).Replace(" ", ""),
                Birthdate = result.Find(r => Regex.IsMatch(r, "(\\d{1,2}\\/\\d{1,2}\\/\\d{4})"))
            };
        }
    }
}