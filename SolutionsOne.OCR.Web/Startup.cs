﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SolutionsOne.OCR.Web.Startup))]
namespace SolutionsOne.OCR.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
